package com.telanox.nexgen.vistaViewConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Config {

	private static final Logger logger = LogManager.getLogger(Config.class);

	private static final String CONFIG_FILE = "VistaViewConverterConfig.xml";
	private static final String ATTRIBUTE_SPECIFIER = "@";
	private static final String PATH_SEPARATOR = ".";
	private static final String PATH_SEPARATOR_REGEX = "\\.";
	
	
	private XMLConfiguration xmlConfig = null;
	private List<String> nodes = new ArrayList<>();
	/*
	 * In some of the following declerations, instead of creating a Pair class the Map.Entry<> has been conveniently used to hold a pair
	 */
	private Map<String, String> nodesAndPaths = new HashMap<>(); // node and the corresponding path
	private Map<String, String> pathAndNodes = new HashMap<>(); // this is the reverse of the above map. Maps the paths to the corresponding node. Note: There is a one-to-one relation between node and path
	private Map<String, String> mergeWith = new HashMap<>(); // for each node it identifies which node its output will merge with. E.g. an entry <Property2, Property> means that info for Property2 should go to same file as Property
	private Map<String, List<KeyValuePair>> nodesPathFieldNames = new LinkedHashMap<>();  // Map<NodeName, Map.Entry<List<Paths>, <FieldNames>> for each node a list of path and corresponding fieldNames. Both lists are of the same size
    private Map<String, List<KeyValuePair>> nodesFieldsFromAtrributes = new HashMap<>(); // Map<NodeName, List<Map.Entry<AttributeName, FieldName>>> basically shows for each node what fields are to be extracted from the attributes
	private Map<String, List<Triple>> path_Attribute_FieldName_Node = new HashMap<>();
    private Set<String> pathsToElements; // all paths that provide value to a field or generate a node. Basically these are all paths we are interested in
	private Set<String> pathsWithAttributes; // field paths that have an attribute specifier in them. In this set the attribute specifier is stripped off
	
	
	private static Config instance = null;
	
	public static Config getInstance() {
		if(instance == null) {
			instance = new Config();
			instance.initialize();
		}
		return instance;
	}
	
	
	
	private void initialize() {
		Configurations configs = new Configurations();
		try {
			xmlConfig = configs.xml(CONFIG_FILE);
			processNodePaths();
			processNodePathsFieldNames();
			createPathsToElements();
			createPathsWithAttributes();
			processNodesFieldsFromAtrributes();
			processPath_Attribute_FieldName_Node();
		} catch (ConfigurationException e) {
			e.printStackTrace();
			return;
		}
	}
	
	/* 
	 * The nodePathFieldName keep the mapping between Paths and FieldNames on a per node basis (hence the nested map)
	 * Since the Path in nodePathFieldName contain the attribute specifier, removing the attribute specifier will not longer keep the path unique
	 * it will now match multiple Fields and we keep those fields in the triple. The stripped off Path will now match a List of triples
	 * After processing this function we will have a map such that given a path (without attribute specifer), find the (attribute, FieldName, node) that match that path. 
	 * the triple here is a tuple of (Attribute, FieldName, Node)
	 */
	private void processPath_Attribute_FieldName_Node() {
		for(Map.Entry<String, List<KeyValuePair>> entry : nodesPathFieldNames.entrySet()) { // start with all paths that are specified in config
			for(KeyValuePair pathFieldNameEntry : entry.getValue()) { // entry.getValues() is a list of all pairs of Paths(which includes attribute specifier) and FieldName
				if (doesPathContainAttributeSpecifier(pathFieldNameEntry.getKey())) {
					String pathWithoutAttribute = getPathWithoutAttributeSpecifier(pathFieldNameEntry.getKey());
					String attribute = getAttributeFromPath(pathFieldNameEntry.getKey());
					if(pathWithoutAttribute == null || attribute == null) {
						continue;
					}
					Triple val = new Triple(attribute, pathFieldNameEntry.getValue(), entry.getKey());
					if(path_Attribute_FieldName_Node.containsKey(pathWithoutAttribute)) { // check if the result already contains an entry. If it does then add it to that list
						path_Attribute_FieldName_Node.get(pathWithoutAttribute).add(val);
					}
					else { // if the result does not contain an entry then create a list and insert it
						List<Triple> listOfTriples = new ArrayList<>();
						listOfTriples.add(val);
						path_Attribute_FieldName_Node.put(pathWithoutAttribute, listOfTriples);
					}
				}
			}
		}
	}


	private void addToMapWithSetValues(Map<String, Set<String>> map, String key,  String value) {
		Set<String> mapValue = map.get(key);
		if(mapValue == null) {
			mapValue = new HashSet<String>();
			map.put(key, mapValue);
		}
		mapValue.add(value);
	}
	

	private void processNodesFieldsFromAtrributes() {
		for(Map.Entry<String, List<KeyValuePair>> entry : nodesPathFieldNames.entrySet()) {
			// check if any fieldPath has an attribute specifier in it
			List<KeyValuePair> attributeAndFieldNameList =
					entry.getValue().stream()
						.filter(e->e.getKey().contains(ATTRIBUTE_SPECIFIER))
						.filter(e->e.getKey().split(ATTRIBUTE_SPECIFIER)[0].equals(nodesAndPaths.get(entry.getKey()))) //part before the @ should match the node path
						.map(e->new KeyValuePair(e.getKey().split(ATTRIBUTE_SPECIFIER)[1], e.getValue()))
						.collect(Collectors.toList());
			
			if(attributeAndFieldNameList.size() > 0) {
				nodesFieldsFromAtrributes.put(entry.getKey(), attributeAndFieldNameList);
			}
		}
	}

	private void createPathsToElements() {
		pathsToElements = nodesPathFieldNames.entrySet()
			.stream()
			.flatMap(e->e.getValue().stream())
			.map(e->e.getKey())
			.map(e->stripAttributeFromPath(e))
			.collect(Collectors.toSet());
		
		// add the paths to nodes
		nodesAndPaths.entrySet()
			.stream()
			.map(Map.Entry::getValue)
			.collect(Collectors.toCollection(()->pathsToElements));
	}
	private void createPathsWithAttributes() {
		// makes a collection of all the paths that have an attribute specification in it. i.e. it has "@" in it
		pathsWithAttributes = nodesPathFieldNames.entrySet()
			.stream()
			.flatMap(e->e.getValue().stream())
			.map(e->e.getKey())
			.filter(e->e.contains(ATTRIBUTE_SPECIFIER))
			.map(e->stripAttributeFromPath(e))
			.collect(Collectors.toSet());
	}

	
	public  List<String> getNodesThatGenerateCSV(){
		return nodesPathFieldNames.keySet()
					.stream()
					.filter(e->!mergeWith.containsKey(e))
					.collect(Collectors.toList());
		//return new ArrayList<>(nodesPathFieldNames.keySet());
	}

	private String stripAttributeFromPath(String path) {
		return path.split(ATTRIBUTE_SPECIFIER)[0]; // strip off anything following the @ symbol and return the first element 
	}
	private void processNodePathsFieldNames() {
		for(int ii = 0; ii < nodes.size(); ii++) {
			String node = xmlConfig.getString("nodes.node("+ ii + ")[@name]");
			//get all the fields
			List<String> fields = xmlConfig.getList(String.class, "nodes.node(" + ii + ").fields.field[@name]");
			List<String> paths = xmlConfig.getList(String.class, "nodes.node(" + ii + ").fields.field[@path]");
			if(fields.size() != paths.size()) {
				logger.debug("Invalid format for node: " + node);
				continue;
			}
			List<KeyValuePair> pathsFieldNamesList = new ArrayList<>();
			for(int jj = 0; jj < fields.size(); jj++) {
				pathsFieldNamesList.add(new KeyValuePair(paths.get(jj), fields.get(jj)));
			}
			nodesPathFieldNames.put(node, pathsFieldNamesList);

		}
	}
	
	private void processNodePaths() {
		List<?> nodesFromConfig = xmlConfig.getList("nodes.node[@name]");
		for(int ii = 0; ii < nodesFromConfig.size(); ii++) {
			String node = nodesFromConfig.get(ii).toString();
			nodes.add(node);
			String path = xmlConfig.getString("nodes.node("+ii+")[@path]");
			nodesAndPaths.put(node, path);
			String mergeWithNode = xmlConfig.getString("nodes.node("+ii+")[@mergeOutputWith]");
			if(mergeWithNode != null) {
				mergeWith.put(node, mergeWithNode.trim());
			}
		}
	}

	
	@Override
	public String toString() {
		StringBuilder retVal = new StringBuilder();
		String indent = "  ";
		retVal.append("Config:\n");
		retVal.append(indent + "nodes: " + nodes);
		retVal.append("\n");
		retVal.append("NodesAndPaths:\n");
		nodesAndPaths.forEach((k, v)->retVal.append("    " + k + " : " + v + "\n"));
		nodesPathFieldNames.forEach((k,v)->{retVal.append("Node: " + k + "\n"); 
			 								for(int ii = 0; ii< v.size(); ii++) {
			 									retVal.append("    FieldName: " + v.get(ii).getValue() + " Path: " + v.get(ii).getKey() + "\n");
			 								}
			 							   });
		retVal.append("PathToElements:\n");
		pathsToElements.stream().forEach(e->retVal.append("    " + e + "\n"));
		retVal.append("PathsWithAttributes:\n");
		pathsWithAttributes.stream().forEach(e->retVal.append("    " + e + "\n"));
		retVal.append("NodesFieldsFromAtrributes:\n");
		nodesFieldsFromAtrributes.forEach((k,v)->retVal.append(k + " : " + v.toString() + "\n"));
		retVal.append("Path_Attribute_FieldName_Node:\n");
		path_Attribute_FieldName_Node.forEach((k,v)->retVal.append(k + " : " + v.toString() + "\n"));
		retVal.append("MergeWith:\n");
		mergeWith.forEach((k, v)->retVal.append("    " + k + " : " + v + "\n"));
		
		return retVal.toString();
		
	}

	public static String getAttributeFromPath(String path) {
		String[] splitPath = path.split(ATTRIBUTE_SPECIFIER);
		if(splitPath.length < 2) {
			return null;
		}
		return splitPath[1];
	}
	public static String getPathWithoutAttributeSpecifier(String path) {
		if(path == null) {
			return null;
		}
		return path.split(ATTRIBUTE_SPECIFIER)[0];
	}
	public static boolean doesPathContainAttributeSpecifier(String path) {
		return path.contains(ATTRIBUTE_SPECIFIER);
	}
	
	public List<String> getNodes() {
		return nodes;
	}



	public Set<String> getPathsToElements() {
		return pathsToElements;
	}



	public Map<String, String> getNodesAndPaths() {
		return nodesAndPaths;
	}

	public Map<String, List<KeyValuePair>> getNodesFieldsFromAtrributes() {
		return nodesFieldsFromAtrributes;
	}



	public Set<String> getPathsWithAttributes() {
		return pathsWithAttributes;
	}



	public Map<String, List<KeyValuePair>> getNodesPathFieldNames() {
		return nodesPathFieldNames;
	}



	public static String getPathSeparator() {
		return PATH_SEPARATOR;
	}



	public static String getAttributeSpecifier() {
		return ATTRIBUTE_SPECIFIER;
	}



	public static String getPathSeparatorRegex() {
		return PATH_SEPARATOR_REGEX;
	}



	public Map<String, List<Triple>> getPath_Attribute_FieldName_Node() {
		return path_Attribute_FieldName_Node;
	}



	public Map<String, String> getMergeWith() {
		return mergeWith;
	}

}
