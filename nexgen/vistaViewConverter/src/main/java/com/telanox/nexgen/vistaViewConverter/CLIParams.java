package com.telanox.nexgen.vistaViewConverter;

import picocli.CommandLine;
import picocli.CommandLine.Option;
import static picocli.CommandLine.Help.Visibility.ALWAYS;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CLIParams {
	
	private static final Logger logger = LogManager.getLogger(CLIParams.class);


	@Option(names = {"-V", "--version"}, versionHelp = true, description = "display version info")
	boolean versionInfoRequested;

	@Option(names = {"-h", "--help"}, usageHelp = true, description = "display this help message")
	boolean usageHelpRequested;
	
	@Option(names = {"-f", "--files"}, arity = "0..*", description = "space separated list of files to parse")
    private Path[] inputFiles;
	
	@Option(names = {"-o", "--outputDir"}, arity = "0..1", description = "output directory")
	Path outputDir;
	
	@Option(names = {"-i", "--inputDir"}, arity = "0..1", description = "input directory (all xml files in the directory will be parsed")
    Path inputDir;
	
	@Option(names = {"-j", "--join"}, arity = "0..1", description = "join (concat) output for each node type.", showDefaultValue = ALWAYS)
    boolean join = false;
	
	@Option(names = {"-c", "--config"}, arity = "0..1", description = "config file. This option overrides the config file packaged in the application", showDefaultValue = ALWAYS)
    boolean config=false;
	
	@Option(names = {"-n", "--nodes"}, arity = "0..1", description = "space separated list of nodes for which csv should be generated. Options are : Vista, Property, Indicator", showDefaultValue = ALWAYS)
    private String[] nodes;
	
	@Option(names = {"-w", "--withoutHeaders"},  description = "generate CSV file without headers", showDefaultValue = ALWAYS)
    boolean withoutHeaders = false;
	
	@Option(names = {"-p", "--prefix"}, arity = "0..1",  description = " when join is specified this is file output prefix per node type e.g. Vista=output_Vista Property=output_Property ")
    String prefix = "output";
	
	@Option(names = {"-e", "--extension"}, arity = "0..1",  description = " file extension of the output file ")
    String outputExtension = ".csv";
	
	@Option(names = {"--overwrite"}, arity = "0..1",  description = " overwrite output file if previous one exists ")
    boolean overwrite = false;

	Path[] fileList = null; // this is a consolidate list of files which come either from inputFiles parameter or input dir parameter
	
	public Path[] getInputFiles() {
		return inputFiles;
	}
	
	public Path[] getFileList() {
		return fileList;
	}
	/*
	 * If join is specified then the output if generated to files of format <prefix>_<node>.csv e.g. output_Vista.csv, output_Property.csv, etc
	 * 		- if output folder is specified then output files are created there
	 *      - if output folder is not specified 
	 *      		- if input folder is specified then output files are created there
	 *      		- else throw an error
	 */
	public boolean processCLI() {
		try {
			if(isJoin() && outputDir == null && inputDir == null) {
				System.out.println("With join option either output directory or input directory must be specified)");
				CommandLine.usage(this, System.out);
				return false;
			}
			if((inputFiles == null || inputFiles.length == 0) && inputDir == null) {
				System.out.println("Either valid file name or a folder containing files must be specified");
				CommandLine.usage(this, System.out);
				return false;
			}
			
			// consolidate the files from either of the 2 sources : input dir or files specified on command line
			if(inputFiles == null || inputFiles.length == 0) {
				// check if input directory is specified
				if(inputDir != null) {
					if(inputDir.toFile().isDirectory()) {
						// get all the .xml files in the directory
						fileList =  Files.list(inputDir)
										.filter(e->e.getFileName().toString().endsWith(".xml")).toArray(Path[]::new);
					}
				}
			}
			else {
				fileList = inputFiles;
			}
			
			StringBuilder msg = new StringBuilder(); 
			Arrays.stream(fileList).forEach(e->msg.append(e.toAbsolutePath().toString() + "\n"));
			logger.debug("Files to process: \n" + msg.toString());
			return true;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	


	@Override
	public String toString() {
		return  "CLIParams :" 
					+ "\n    inputFiles=" + Arrays.toString(inputFiles) 
					+ "\n    outputDir=" + outputDir
					+ "\n    inputDir=" + inputDir
					+ "\n    config=" + config
					+ "\n    join=" + join
					+ "\n    nodes=" + Arrays.toString(nodes)
					+ "\n    withoutHeaders=" + withoutHeaders
					+ "\n    prefix=" + prefix
					+ "\n    outputExtension=" + outputExtension
					+ "\n    overwrite=" + overwrite
					+ "\n";
	}

	public boolean isVersionInfoRequested() {
		return versionInfoRequested;
	}

	public boolean isUsageHelpRequested() {
		return usageHelpRequested;
	}

	public Path getOutputDir() {
		return outputDir;
	}

	public Path getInputDir() {
		return inputDir;
	}

	public boolean isJoin() {
		return join;
	}

	public boolean isConfig() {
		return config;
	}
	
	public String[] getNodes() {
		return nodes;
	}

	public boolean isWithoutHeaders() {
		return withoutHeaders;
	}

	public static Logger getLogger() {
		return logger;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getOutputExtension() {
		return outputExtension;
	}

	public boolean isOverwrite() {
		return overwrite;
	}

}
