package com.telanox.nexgen.vistaViewConverter;

import java.util.HashMap;
import java.util.Map;
/*
 * This class holds the data collected from the xml parser. It contains all the fields corresponding to the node as defined in the config file
 */
public class Node {

	private String nodePath;
	private String nodeName;
	
	Map<String, Field> fields = new HashMap<>();
	
	

	public  Node(String nodePath, String nodeName) {
		this.nodePath = nodePath;
		this.nodeName = nodeName;
	}

	


	public void addField(Field field) {
		fields.put(field.getName(), field);
	}
	


	public String getNodePath() {
		return nodePath;
	}




	public void setNodePath(String nodePath) {
		this.nodePath = nodePath;
	}




	public String getNodeName() {
		return nodeName;
	}




	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}




	@Override
	public String toString() {
		StringBuilder retVal = new StringBuilder("Node: \n    nodePath=" + nodePath + "\n    nodeName=" + nodeName + "\n    fields:") ;
		fields.forEach((k,v)->retVal.append("\n        "+v.toString()));
		return retVal.toString();
	}




	public Field getField(String fieldName) {
		return fields.get(fieldName);
	}


	
}
