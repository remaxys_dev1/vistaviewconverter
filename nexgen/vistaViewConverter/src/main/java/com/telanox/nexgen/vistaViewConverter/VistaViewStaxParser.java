package com.telanox.nexgen.vistaViewConverter;

import java.io.IOException;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class VistaViewStaxParser {
	private Processor processor = null;
	XMLEventReader xmlEventReader = null;
	DataHandler dataHandler = null;

	public VistaViewStaxParser(Processor processor, XMLEventReader reader, DataHandler dataHandler) {
		this.processor = processor;
		this.xmlEventReader = reader;
		this.dataHandler = dataHandler;

	}

	public boolean startParsing() throws IOException {
		dataHandler.beginProcessing();
		while (xmlEventReader.hasNext()) {
			try {
				XMLEvent event = xmlEventReader.nextEvent();
				if (event.isStartElement()) {
					handleStartElement(event.asStartElement());
				}
				else if(event.isEndElement()) {
					handleEndElement(event.asEndElement());
				}
				else if(event.isCharacters()) {
					handleCharacters(event.asCharacters());
				}
			} catch (XMLStreamException e) {
				e.printStackTrace();
				return false;
			}
		}
		dataHandler.endProcessing();
		return true;
	}

	private void handleCharacters(Characters characters) {
		if(processor != null) {
			processor.processCharacters(characters);
		}
	}

	private void handleEndElement(EndElement element) {
		processor.processEvent(element);
	}

	private void handleStartElement(StartElement element) {
		processor.processEvent(element);
	}
}
