package com.telanox.nexgen.vistaViewConverter;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import picocli.CommandLine;

/**
 *
 */
public class VistaViewConverter {
	private static final Logger logger = LogManager.getLogger(VistaViewConverter.class);
	
	public static void main(String[] args) {
		
		try {
			
			CLIParams params = parseCLI(args);
			if(params == null || !params.processCLI()) {
				return;
			}
			logger.info(params);
			
			logger.info(Config.getInstance());
			XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

			
			DataHandler dataHandler = null;
			if(params.isJoin()) {
				// use the same set of outfiles. 
				
			}
			for (Path inputFile : params.getFileList()) {
				if(params.isJoin()) { 
					if(dataHandler == null) {// in case of join the data handler is only created once
						// For creating output files: if output dir is not null then use it, else use input dir 
						dataHandler = new CSVGenerator(getWriters(params.getPrefix(), params.getOutputExtension(), params.isOverwrite(), params.getOutputDir() == null ? params.getInputDir() : params.getOutputDir()));
					}
				}
				else { // output files are created for each individual input file
					dataHandler = new CSVGenerator(getWriters(inputFile.getFileName().toString().split("\\.")[0], 
											params.getOutputExtension(), 
											params.isOverwrite(), 
											params.getInputDir() != null ? params.getOutputDir() : inputFile.getParent()));
				}
					
				XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(Files.newInputStream(inputFile));
				VistaViewStaxParser parser = new VistaViewStaxParser(new XMLProcessor(dataHandler), xmlEventReader, dataHandler);
				parser.startParsing();
			}
		} 
		catch(FileAlreadyExistsException e) {
			System.out.println("Output file alredy exists: " + e.getMessage());
			System.out.println("Remove the file and try again");
			return;
		}
		catch (XMLStreamException | IOException e) {
			e.printStackTrace();
		} 
		
		logger.info("Completed Parsing");
	}
	
	private static CLIParams parseCLI(String[] args) {
		CLIParams params;
		try {
			 params = CommandLine.populateCommand(new CLIParams(), args);
		} catch (Exception e) { // catch the missing parameter exception and display the usage and terminate the program
			logger.info(e.getMessage());
			CommandLine.usage(new CLIParams(), System.out);
			return null;
		}
		if (params.usageHelpRequested) {
		   CommandLine.usage(params, System.out);
		   return null;
		}
		return params;
	}
	
	private static Map<String, Writer> getWriters(String filePrefix, String fileExtension, boolean overwrite, Path directory) throws IOException{
		Map<String, Writer>retVal = new HashMap<>();
		StandardOpenOption[] openOptions;
		for(String node : Config.getInstance().getNodesThatGenerateCSV()) {
			if(overwrite) {
				openOptions = new StandardOpenOption[] {StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING};
			}
			else {
				openOptions = new StandardOpenOption[] {StandardOpenOption.CREATE_NEW};
			}
			retVal.put(node, Files.newBufferedWriter(directory.resolve(filePrefix  + "_" + node + fileExtension), openOptions));
		}
		return retVal;
	}
	
	
}
