package com.telanox.nexgen.vistaViewConverter;

import java.io.IOException;
import java.io.Writer;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CSVGenerator implements DataHandler{

	private static final Logger logger = LogManager.getLogger(CSVGenerator.class);

	private Map<String, CSVPrinter> nodesCSVPrinterMap = new HashMap<>();
	private Map<String, String[]> nodesAndHeaders = new HashMap<>();
	private Map<String, Writer> outputWriters = null;
	private boolean isInitialized = false;
	
	public CSVGenerator(Map<String, Writer> outputWriters) {
		this.outputWriters = outputWriters;
		// generate the header for each node
		for(String node : Config.getInstance().getNodesThatGenerateCSV()) {
			List<KeyValuePair> pathsFieldNames = Config.getInstance().getNodesPathFieldNames().get(node);
			String[] headerValues = new String[pathsFieldNames.size()];
			for(int ii = 0; ii <  pathsFieldNames.size(); ii++) {
				headerValues[ii] = pathsFieldNames.get(ii).getValue();
			}
			nodesAndHeaders.put(node, headerValues);
		}
	}

	@Override
	public void beginProcessing() throws IOException {
		if (!isInitialized) {
			for (Map.Entry<String, Writer> entry : outputWriters.entrySet()) {
				nodesCSVPrinterMap.put(entry.getKey(), new CSVPrinter(entry.getValue(),
						CSVFormat.DEFAULT.withHeader(nodesAndHeaders.get(entry.getKey()))));
			} 
		}	
		isInitialized = true;
	}


	@Override
	public void endProcessing() {
		logger.debug("End Processing is called");
	    nodesCSVPrinterMap.forEach((node, printer)->{
			try {
				printer.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
				
		
	}

	@Override
	public void handleNode(Node node) {
		String nodeNameToUse = Config.getInstance().getMergeWith().containsKey(node.getNodeName()) 
										? Config.getInstance().getMergeWith().get(node.getNodeName()) 
										: node.getNodeName();
												
		CSVPrinter printer = nodesCSVPrinterMap.get(nodeNameToUse);
		if(printer == null) {
			logger.debug("No CSV Printr for node type: " + node.getNodeName());
			return;
		}
		String[] headerFieldNames = nodesAndHeaders.get(nodeNameToUse);
		if(headerFieldNames == null) {
			logger.debug("No headers found for node type: " + node.getNodeName());
			return;
		}
		try {
		for(String fieldName : headerFieldNames) {
			Field field = node.getField(fieldName);
			if(field == null) {
				printer.print("");
			}
			else {
				printer.print(field.getValue());
			}
		}
		printer.println();
		} catch(IOException e) {
			// TODO: Handle exception
			e.printStackTrace();
		}
	}
	
	
	
}
