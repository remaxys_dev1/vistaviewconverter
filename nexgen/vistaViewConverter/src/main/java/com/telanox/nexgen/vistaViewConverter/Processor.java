package com.telanox.nexgen.vistaViewConverter;


import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;

public interface Processor {
	public void processEvent(StartElement event);
	public void processEvent(EndElement event);
	public void processCharacters(Characters event);
}
