package com.telanox.nexgen.vistaViewConverter;

public class Field {

	private String name;
	private String value;
	
	public Field(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public Field(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value.replaceAll("\n", " ");
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Field [name=" + name + ", value=" + value + "]";
	}
}
