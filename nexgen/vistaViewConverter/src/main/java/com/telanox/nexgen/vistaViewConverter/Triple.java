package com.telanox.nexgen.vistaViewConverter;

public class Triple {
	String value0;
	String value1;
	String value2;
	
	public Triple (String value0, String value1, String value2) {
		this.value0 = value0;
		this.value1 = value1;
		this.value2 = value2;
	}

	public String getValue0() {
		return value0;
	}

	public void setValue0(String value0) {
		this.value0 = value0;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	
	@Override
	public String toString() {
		return "[" + value0 + ", " + value1 + ", " + value2 + "]";
	}
}
