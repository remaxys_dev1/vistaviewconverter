package com.telanox.nexgen.vistaViewConverter;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class XMLProcessor implements Processor {
	
	private static final Logger logger = LogManager.getLogger(XMLProcessor.class);

	
	Stack<StartElement> elementStack = new Stack<>();
	Stack<Node> nodeStack = new Stack<>();
	private Field emptyField;
	private String emptyFieldPath;
	private StringBuilder dataAccumulator;
	private DataHandler dataHandler;
	
	public XMLProcessor(DataHandler dataHandler) {
		this.dataHandler = dataHandler;
	}

	/*
	 * When processing the start element, following needs to happen
	 *  - create a new node if required
	 *  - Since the only data contained in the startElement is in the attributes therefore
	 *  		- Fill fields of the current node that come from an attribute
	 *  		- Fill fields of the current node that come from attributes of ancestral elements since those elements will be on the stack
	 *  - If the startElement corresponds to a field in a node then check if the value comes from an attribute, in that case create those fields 
	 * (non-Javadoc)
	 * @see com.telanox.nexgen.vistaViewConverter.Processor#processEvent(javax.xml.stream.events.StartElement)
	 */
	@Override
	public void processEvent(StartElement element) {
		elementStack.push(element);
		String stackPath = getStackPath();
		
		/*
		 * Make a check up front to see if the element is interesting to us. i.e. whether it will result in a new node or a new property or a future property
		 */
		if(!Config.getInstance().getPathsToElements().contains(stackPath)) {
			return;
		}
		// check if its a new node that needs to be emitted
		Optional<String> node = Config.getInstance().getNodesAndPaths().entrySet()
											.stream()
											.filter(e->e.getValue().equals(stackPath))
											.map(e->e.getKey())
											.findFirst();

		if(node.isPresent()) {
			//logger.debug("Found New node: " + node.get() + " for path: " + stackPath);
			Node currentNode = new Node(stackPath, node.get());
			nodeStack.push(currentNode);
			
			// set fields from attributes in the node if any
			List<KeyValuePair> fieldAndAttributesList =  Config.getInstance().getNodesFieldsFromAtrributes().get(node.get()); // ths list is Map.Entry<Attribute, FieldName>>
			if(fieldAndAttributesList != null && fieldAndAttributesList.size() > 0) {
				Iterator<Attribute> iter = element.getAttributes();
				while(iter.hasNext()) {
					Attribute attr = iter.next();
					// check if this attribute corresponds to a field
					Optional<KeyValuePair> match = fieldAndAttributesList.stream().filter(e->e.getKey().equals(attr.getName().getLocalPart())).findFirst();
					if(match.isPresent()) {
						//logger.debug("Generate field from : " + match.get().toString() + " value: " + attr.getValue());
						Field field = new Field(match.get().getValue(), attr.getValue());
						currentNode.addField(field);
					}
				}
			}
			
			// take care of the case of single element node e.g. the case of VMResolver. In this case the same element generates the node and all its fields
			
			
			// get fields of this node that use fields from an ancestor node (e.g. Property also has a field for Wid of Vista)
			List<Triple> pathFieldNames = // at the end of this transformation this will contain the list of all fields whose value must from an ancestor node
					Config.getInstance().getNodesPathFieldNames().get(node.get())
								.stream()
								.map(e->new Triple(Config.getPathWithoutAttributeSpecifier(e.getKey()), e.getKey(), e.getValue())) // strip off anything after @
								.filter(e->(e.getValue0().length() < stackPath.length()) && stackPath.startsWith(e.getValue0())) // only allow field whose value come from an ancestor node
								.collect(Collectors.toList());
			// for each element of the list, get the field value from the ancestor
			for(Triple triple : pathFieldNames) {
				StartElement elementFromStack = getElementFromStack(triple.getValue0());
				if(elementFromStack != null) {
					Field fieldFromAncestor = getFieldFromAncestorStackElement(elementFromStack, triple.getValue1(), triple.getValue2());
					if(fieldFromAncestor != null) {
						//logger.debug("Ancestor Field" + fieldFromAncestor);
						currentNode.addField(fieldFromAncestor);
					}
				}
			}
			
		}
		else { // if the code reaches this point then this element corresponds to a field i.e. it will not result in the creation of a new node
			// get fields that uses an attribute from this element
			logger.trace("Found Regular field: " + element.getName().getLocalPart());
			List<Triple> fieldInfoList = Config.getInstance().getPath_Attribute_FieldName_Node().get(stackPath);
			// the path can match a field in 2 two ways. 
			//    - one as part of path specification with an attribute specifier
			//    - two as path to a regular field (without any attribute specifier)
			// A path can satisfy both the above conditions i.e. it can specify a field directly as well as through an attribute
			if(fieldInfoList != null && fieldInfoList.size() > 0) { // this is to check condition one described above
				for(Triple fieldInfo : fieldInfoList) {
					Field field = createField(element, fieldInfo.getValue0(), fieldInfo.getValue1());
					if(field != null) {
						if(!nodeStack.isEmpty()) {
							nodeStack.peek().addField(field); // add it to the node at the top of the stack
							//logger.debug("Added field to the top of the node stack: " + field);
						}
					}
				}
			}
		}
			// moved the following out of the else block to take care of the case when the element results in a new node as well as is a field value.
			//this is to check condition two described above. Note we are not using if-else since both conditions can be applicable. 
			//   check if the path exists as is (without any stripping, etc). If it does then create a field corresponding to it.
			// since the paths are on a per node basis, need to check if a node exists
			if(nodeStack.isEmpty()) {
				return;
			}
			// KeyValuePair = (Path, FieldName)
			Optional<KeyValuePair> fieldForPath = Config.getInstance().getNodesPathFieldNames().get(nodeStack.peek().getNodeName())
														.stream()
														.filter(e->e.getKey().equals(stackPath))
														.findFirst();
			if(fieldForPath.isPresent()) { // create field and add to the node on the top of the nodestack
				emptyField = new Field(fieldForPath.get().getValue()); 
				emptyFieldPath = stackPath;
				dataAccumulator = new StringBuilder(); // should  collect data for this field since its value did not come from the attribute
			}
		
		return;
	}
	
	private Field createField(StartElement element, String attribute, String fieldName) {
		if(attribute == null) {
			return null;
		}
		Iterator<Attribute> iter = element.getAttributes();
		while(iter.hasNext()) {
			Attribute attr = iter.next();
			if(attribute.equals(attr.getName().getLocalPart())){
				return new Field(fieldName, attr.getValue());
			}
		}
		return null;
	}

	private Field getFieldFromAncestorStackElement(StartElement elementFromStack, String fieldPath, String fieldName) {
		String attribute = Config.getAttributeFromPath(fieldPath);
		return createField(elementFromStack, attribute, fieldName);
	}
	
	

	private StartElement getElementFromStack(String stackPath) {
		String[] stackElements = stackPath.split(Config.getPathSeparatorRegex());
		if(stackElements == null || stackElements.length >= elementStack.size()) { // we check for equal to also since we are only looking for ancestral elements
			return null;
		}
		for(int ii = 0; ii< stackElements.length; ii++) {
			if(!elementStack.get(ii).getName().getLocalPart().equals(stackElements[ii])) {
				return null;
			}
		}
		return elementStack.get(stackElements.length -1); // once very element of the path matches then return the element in the stack corresponding to the path
	}

	public String getStackPath() {
		if(elementStack.isEmpty()) {
			return "";
		}
		StringBuilder retVal = new StringBuilder();
		for(int ii=0; ii<elementStack.size() -1; ii++) { // stop one short so that there is no extra "." at the end of path
			retVal.append(elementStack.get(ii).getName().getLocalPart());
			retVal.append(Config.getPathSeparator());
		}
		retVal.append(elementStack.peek().getName().getLocalPart());
		return retVal.toString();
	}

	@Override
	public void processEvent(EndElement event) {
		if(!elementStack.isEmpty()) {
			String stackPath = getStackPath();
			if(emptyFieldPath != null && stackPath.equals(emptyFieldPath) && emptyField != null) {
				emptyField.setValue(dataAccumulator == null ? null : dataAccumulator.toString().trim());
				if(!nodeStack.isEmpty()) {
					nodeStack.peek().addField(emptyField);
				}
				emptyField = null;
				dataAccumulator = null;
			}
			if(!nodeStack.isEmpty() && nodeStack.peek().getNodePath().equals(stackPath)) {
				Node node = nodeStack.pop();
				logger.debug("Popped the following node:\n" + node);
				dataHandler.handleNode(node);
			}
			elementStack.pop();
		}
		return;
	}


	@Override
	public void processCharacters(Characters event) {
		if(!nodeStack.isEmpty() && dataAccumulator != null) {
			dataAccumulator.append(event.getData());
		}
		return;
	}

}
