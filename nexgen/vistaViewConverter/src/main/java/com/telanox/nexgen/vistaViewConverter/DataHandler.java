package com.telanox.nexgen.vistaViewConverter;

import java.io.IOException;
import java.util.Map;

/*
 * Since the headers of the files are not known in advance, this class will accumulate all the data to create the common header
 */
public interface DataHandler {
	
	public void beginProcessing() throws IOException;
	public void handleNode(Node node); 
	public void endProcessing();
}
